<?php
/**
 * Created by PhpStorm.
 * User: kohbo
 * Date: 11/24/2015
 * Time: 18:52
 */

namespace GradeGame;

require_once "User.php";

class Canvas{
    private $token;
    public $user;

    function __construct($access_token)
    {
        $this->token = $access_token;
        $this->user = $this->getUser();
    }

    /**
     * @param $options
     * @return mixed
     * @throws \Exception
     */
    function getCurl($options){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["Authorization: Bearer " . $this->token]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt_array($curl, $options);
        if($curl) {
            $response = curl_exec($curl);
            if(curl_error($curl)){
                throw new \Exception(curl_error($curl));
            } else {
                $response = json_decode($response);
                if(false){
                    throw new \Exception(print_r($response->errors, true));
                } else {
                    return $response;
                }
            }
        } else {
            throw new \Exception("Unable to init cURL");
        }
    }

    /**
     * @return mixed
     * @throws \Exception
     * @internal param $courseID
     * @internal param bool|true $json
     */
    function getCourseAssignments(){
        foreach($this->user->courses as $course){
            $curl_options = [
                CURLOPT_URL =>
                    "https://fgcu.instructure.com/api/v1/courses/" . $course->id .
                    "/assignments?per_page=100&include[]=submission&include[]=overrides"
            ];
            $response = $this->getCurl($curl_options);
            foreach($response as $assignment){
                $assignment_temp = new Assignment(
                    $assignment->created_at,
                    $assignment->due_at,
                    $assignment->id,
                    $assignment->points_possible,
                    $assignment->name
                );
                if(property_exists($assignment, "submission")){
                    $assignment_temp->submission = new Submission(
                        $assignment->submission->score,
                        $this->getGrader($assignment->course_id, $assignment->submission->grader_id),
                        $assignment->submission->id,
                        $assignment->submission->submitted_at,
                        $assignment->submission->late
                    );
                }
                $course->assignments[] = $assignment_temp;
            }
        }
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    function getCourses(){
        $curl_options = array(
            CURLOPT_URL => "https://fgcu.instructure.com/api/v1/courses?per_page=100"
        );
        $response = $this->getCurl($curl_options);
        foreach($response as $course){
            if(!property_exists($response, "errors")){
                $this->user->courses[] = new Course(
                    $course->course_code,
                    $course->enrollment_term_id,
                    $course->id,
                    $course->name,
                    $course->start_at,
                    $course->end_at
                );
            }
        }
    }

    function getAssignmentGrade($courseID,$assignmentID){
        $curl_options = array(
            CURLOPT_URL => "https://fgcu.instructure.com/api/v1/courses/".$courseID."/assignments/".$assignmentID."/submissions/self"
        );
        $curl = $this->getCurl($curl_options);
        $response = curl_exec($curl);

        if(curl_error($curl)){
            throw new \Exception(curl_error($curl));
        } else {
            $response = json_decode($response);
            return $response;
        }
    }

    function getUser(){
        $curl_options = array(
            CURLOPT_URL => "https://fgcu.instructure.com/api/v1/users/self"
        );
        $response = $this->getCurl($curl_options);
        return new User($response->id, $response->name, $response->avatar_url);
    }

    function getCourseUser($courseID, $userID){
        $curl_options = array(
            CURLOPT_URL => "https://fgcu.instructure.com/api/v1/courses/".$courseID."/users/" . $userID
        );
        $response = $this->getCurl($curl_options);
        return new User($response->id, $response->name, $response->avatar_url);
    }

    /**
     * @param $courseID
     * @param $graderID
     * @return User
     */
    function getGrader($courseID, $graderID){
        $graders = $this->user->courses->graders;
        if(!is_null($graders)){
            foreach($graders as $grader){
                if($grader->id == $graderID){
                    return $grader;
                }
            }
        }
        $new_grader = $this->getCourseUser($courseID, $graderID);
        $graders[] = $new_grader;
        return $new_grader;
    }

    function build(){
        $this->getCourses();
        $this->getCourseAssignments();
    }

    function __toString()
    {
        return json_encode($this);
    }
}
