<?php
/**
 * Created by PhpStorm.
 * User: kohbo
 * Date: 11/25/2015
 * Time: 14:03
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Fusion Charts</title>
    <script src="assets/fusioncharts/js/fusioncharts.js"></script>
    <script src="assets/fusioncharts/js/fusioncharts.charts.js"></script>
    <script src="assets/fusioncharts/js/themes/fusioncharts.theme.carbon.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,900' rel='stylesheet' type='text/css'>
</head>
<style>
    html {
        background-color: black;
    }
</style>
<body>
<div id="chart">Chart Div</div>
</body>
<script>
    $(document).ready(function(){
        $.getJSON("http://do.kohding.net/GradeGame.php").done(function(json_data){
            var chart = {
                "chart" : {
                    "caption"       : "Grades",
                    "xAxisName"     : "Assignment",
                    "yAxisName"     : "Grade",
                    "theme"         : "carbon",
                    "bgAlpha"       : "100",
                    "canvasBgAlpha" : "100",
                    "bgColor"       : "#000000",
                    "canvasBgColor" : "#000000",
                    "baseFontColor" : "#dddddd",
                    "baseFont"      : "Roboto",
                    "baseFontSize"  : 12,
                    "labelDisplay"  : 'ROTATE'

                },
                "data" : json_data.data
            };
            var fusion = new FusionCharts({
                "type" : "column2d",
                "renderAt" : "chart",
                "dataFormat" : "json",
                "dataSource" : chart,
                "width" : (window.innerWidth-100).toString(),
                "height" : (window.innerHeight-50).toString()
            });

            fusion.render();
        });
    });
</script>
</html>
