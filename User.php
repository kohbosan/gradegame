<?php
/**
 * Created by PhpStorm.
 * User: kohbo
 * Date: 11/24/2015
 * Time: 22:56
 */

namespace GradeGame;

require_once "Course.php";

class User
{
    public $id;
    public $name;
    public $avatar;
    public $courses;

    function __construct($id, $name, $avatar)
    {
        $this->id = $id;
        $this->name = $name;
        $this->avatar = $avatar;
    }

    function __toString()
    {
        // TODO: Implement __toString() method.
        return "";
    }
}
