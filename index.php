<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Fusion Charts</title>
    <script src="assets/fusioncharts/js/fusioncharts.js"></script>
    <script src="assets/fusioncharts/js/fusioncharts.charts.js"></script>
    <script src="assets/fusioncharts/js/themes/fusioncharts.theme.fint.js"></script>
</head>
<body>
<div id="chart">Chart Div</div>
</body>
</html>