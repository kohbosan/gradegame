<?php
/**
 * Created by PhpStorm.
 * User: kohbo
 * Date: 11/25/2015
 * Time: 01:14
 */

namespace GradeGame;


class Submission
{
    public $score;
    public $grader;
    public $id;
    public $submitted;
    public $late;
    public $comments;

    function __construct(
        $score,
        $grader,
        $id,
        $submitted,
        $late
        )
    {
        $this->score = $score;
        $this->grader = $grader;
        $this->submitted = $submitted;
        $this->late = $late;
        $this->id = $id;
    }
}