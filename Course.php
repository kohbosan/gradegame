<?php
/**
 * Created by PhpStorm.
 * User: kohbo
 * Date: 11/25/2015
 * Time: 00:26
 */

namespace GradeGame;

require_once "Assignment.php";

class Course
{
    public $code;
    public $term;
    public $id;
    public $name;
    public $start_at;
    public $end_at;
    public $assignments;
    public $graders;

    function __construct(
        $code,
        $term,
        $id,
        $name,
        $start_at,
        $end_at
    )
    {
        $this->end_at = $end_at;
        $this->code = $code;
        $this->term = $term;
        $this->id = $id;
        $this->name = $name;
        $this->start_at = $start_at;
    }

    function getCourseGradeJSON(){
        $grades = [];
        if(!is_null($this->assignments)){
            foreach($this->assignments as $assignment){
                if(!is_null($assignment->submission->score)){
                    $grades[] = [
                        "label" => $assignment->name,
                        "value" => ($assignment->submission->score / $assignment->points_possible)*100
                    ];
                }
            }
        }
        return json_encode(["data" => $grades]);
    }
}