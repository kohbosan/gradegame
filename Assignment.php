<?php
/**
 * Created by PhpStorm.
 * User: kohbo
 * Date: 11/24/2015
 * Time: 23:52
 */

namespace GradeGame;

require_once "Submission.php";

class Assignment
{
    public $created;
    public $due;
    public $id;
    public $points_possible;
    public $name;
    public $submission;

    function __construct(
        $created,
        $due,
        $id,
        $points_possible,
        $name
    )
    {
        $this->created = $created;
        $this->due = $due;
        $this->id = $id;
        $this->points_possible = $points_possible;
        $this->name = $name;
    }
}